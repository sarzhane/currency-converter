package com.example.currencyconverter.presentation.activity;


import android.annotation.SuppressLint;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.currencyconverter.R;
import com.example.currencyconverter.presentation.base.BaseActivity;
import com.example.currencyconverter.presentation.base.BasePresenter;

public class MainActivity extends BaseActivity implements MainContract.View {
    MainContract.Presenter presenter;
    EditText sum;
    EditText res;
    Spinner spin1;
    Spinner spin2;


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new MainPresenter();
        sum = findViewById(R.id.valueForConvert);
        res = findViewById(R.id.convertResult);
        spin1 = findViewById(R.id.spinner);
        spin2 = findViewById(R.id.spinner2);
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void showResult(double val) {
        res.setText(String.format("%f",val));

    }

    public void convert(View view){
        String val1 = spin1.getSelectedItem().toString();
        String val2 = spin2.getSelectedItem().toString();
        String value= sum.getText().toString();
       presenter.eventOperation(value,val1,val2);


    }
}
