package com.example.currencyconverter.presentation.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(getLayoutRes());
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        onStartView();
    }


    protected abstract int getLayoutRes();
    protected abstract void initView();
    protected abstract void onStartView();
    protected abstract void onDestroyView();
    protected abstract BasePresenter getPresenter();
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().detachView();
        }
    }
}
