package com.example.currencyconverter.presentation.activity;

import com.example.currencyconverter.presentation.Converter;

public class MainPresenter implements MainContract.Presenter {
    MainContract.View view;

    public MainPresenter() {

    }
    @Override
    public void startView(MainContract.View view) {
        this.view = view;
    }
    @Override
    public void eventOperation(String value,String val1,String val2) {
        double val = Double.parseDouble(value);
        double res = Converter.getInstance().convertValue(val1,val2,val);
        view.showResult(res);

    }



    @Override
    public void detachView() {
        if (view != null) view = null;
    }
}
