package com.example.currencyconverter.presentation.activity;

import com.example.currencyconverter.presentation.base.BasePresenter;

public interface MainContract {
    interface View {
        void showResult(double val);
//        void convert();
    }

    interface Presenter extends BasePresenter<View> {
        void eventOperation(String value,String val1,String val2);
    }
}
