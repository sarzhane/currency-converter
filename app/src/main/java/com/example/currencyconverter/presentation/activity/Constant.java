package com.example.currencyconverter.presentation.activity;

public abstract class Constant {
    public static final Double UA_TO_USD = 25.0;
    public static final Double UA_TO_EUR = 30.0;
    public static final Double UA_TO_UA = 1.0;
    public static final Double USD_TO_UA = 0.15;
    public static final Double USD_TO_USD = 1.0;
    public static final Double USD_TO_EUR = 0.9;
    public static final Double EUR_TO_UA = 0.1;
    public static final Double EUR_TO_USD = 1.1;
    public static final Double EUR_TO_EUR = 1.0;
}
