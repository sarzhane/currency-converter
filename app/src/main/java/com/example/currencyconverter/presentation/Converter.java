package com.example.currencyconverter.presentation;

import com.example.currencyconverter.presentation.activity.Constant;

public class Converter {
    private static Converter instance;

    private Converter() {
    }

    public static synchronized Converter getInstance() {
        if (instance == null) {
            instance = new Converter();
        }
        return instance;
    }

    public double convertValue(String val1, String val2, double value) {
        double res = 0;
        switch (val1) {
            case "Доллар США":
                switch (val2) {
                    case "Доллар США":
                        res = value * Constant.USD_TO_USD;
                        break;
                    case "Евро":
                        res = value * Constant.USD_TO_EUR;
                        break;
                    case "Гривна":
                        res = value * Constant.USD_TO_UA;
                        break;
                }
                break;
            case "Евро":
                switch (val2) {
                    case "Доллар США":
                        res = value * Constant.EUR_TO_USD;
                        break;
                    case "Евро":
                        res = value * Constant.EUR_TO_EUR;
                        break;
                    case "Гривна":
                        res = value * Constant.EUR_TO_UA;
                        break;
                }
                break;
            case "Гривна":
                switch (val2) {
                    case "Доллар США":
                        res = value * Constant.UA_TO_USD;
                        break;
                    case "Евро":
                        res = value * Constant.UA_TO_EUR;
                        break;
                    case "Гривна":
                        res = value * Constant.UA_TO_UA;
                        break;
                }
                break;
        }
        return res;
    }
}
